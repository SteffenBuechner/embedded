
/*
  Getting Started example sketch for nRF24L01+ radios
  This is a very basic example of how to send data from one node to another
  Updated: Dec 2014 by TMRh20
*/

#include "SPI.h"

#include "vtimer/TimerBase.h"
#include "vtimer/VirtualTimer.h"

class T1 : public VirtualTimer{
	public:
	T1()
		: VirtualTimer(VirtualTimer::REPEAT)
	{

	}

	void execute(){
		Serial.println("T1");
	}
};

class T2 : public VirtualTimer{
	public:
	T2()
		: VirtualTimer(VirtualTimer::ONE_SHOT)
	{

	}

	void execute(){
		Serial.println("T2");
	}
};

T1 t1;
T2 t2;

void setup() {
  Serial.begin(115200);
  TimerBase::begin();
  t1.begin();
  t2.begin();
  t1.start(1000);
  t2.start(5000);
}


int run = 0;
void loop() {
	Serial.print("Hello Nr:");
	Serial.println(run++);
	Serial.println("Yippieh!");
	t2.start(5000);
	delay(10000);
} // Loop

