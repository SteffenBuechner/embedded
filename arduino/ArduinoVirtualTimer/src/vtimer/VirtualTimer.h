/*
 * =====================================================================================
 *
 *       Filename:  VirtualTimer.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  30.03.2016 12:21:31
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef VirtualTimer_h_
#define VirtualTimer_h_

#include "TimerBase.h"

class VirtualTimer{
	public:
		enum Type{
			REPEAT = 0,
			ONE_SHOT = 1
		};

		virtual void execute() = 0;

		VirtualTimer(Type type):
			type(type),enabled(false),registered(false){
		}

		void begin(){
			TimerBase::registerVirtualTimer(this);
			enabled = false;
		}
		
		void start(int ms){
			startMS = ms;
			remainingMS = ms;
			enabled = true;
		}

		bool update(long ms){
			if(ms > remainingMS){
				if(type == REPEAT){
					remainingMS = startMS;
				}else{
					enabled = false;
					remainingMS = startMS;
				}
				execute();
				return true;
			}else{
				remainingMS -= ms;
			}
		
			return false;
		}

		void stop(){
			enabled = false;
		}

		void setRegistered(bool registered){
			this->registered = registered;
		}

		bool isEnabled(){
			return enabled;
		}

		bool isRegistered(){
			return registered;
		}

	private:
		Type type;
		bool enabled;
		bool registered;
		long remainingMS;
		long startMS;
};

#endif
