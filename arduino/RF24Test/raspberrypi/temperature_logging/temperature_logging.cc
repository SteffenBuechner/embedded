/*
   Copyright (C) 2011 J. Coliz <maniacbug@ymail.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   version 2 as published by the Free Software Foundation.

   03/17/2013 : Charles-Henri Hallard (http://hallard.me)
   Modified to use with Arduipi board http://hallard.me/arduipi
   Changed to use modified bcm2835 and RF24 library
   TMRh20 2014 - Updated to work with optimized RF24 Arduino library

*/

/**
 * Example RF Radio Ping Pair
 *
 * This is an example of how to use the RF24 class on RPi, communicating to an Arduino running
 * the GettingStarted sketch.
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <unistd.h>
#include <RF24/RF24.h>
#include <math.h>
#include <sys/inotify.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <poll.h>
//#include "GPIO.h"

using namespace std;
//
// Hardware configuration
// Configure the appropriate pins for your connections

RF24 radio(RPI_V2_GPIO_P1_15, RPI_V2_GPIO_P1_24, BCM2835_SPI_SPEED_8MHZ);

//I'm the sink
const uint64_t sinkAddress = 0;

// Radio pipe addresses for the 2 nodes to communicate.
int numberOfNodes = 1;
const uint8_t nodesAddress = 1;
const int channel = 3;

const int codeLength = 4;
uint8_t commCode[codeLength] = {0, 1, 2, 3};

/* communication struct */
const uint8_t GET_TEMP = 4;

const uint16_t MAX_UINT16 = (uint16_t)-1;

/* communication struct */
struct command_t {
  	command_t(){
    	this->nodeid = -1;
    	this->cmd = 0;
    	this->state = 0;

		memcpy(this->code, commCode, codeLength);
  	}

  	bool checkCode(){
		bool ok = true;
    	for(int i = 0; i < codeLength && ok; i++)
      		ok &= this->code[i] == commCode[i];

    	return ok;
  	}

  	uint8_t nodeid;
  	uint8_t cmd; /* 0 set, 1 get */
  	uint16_t state;
  	uint8_t code[codeLength];
} __attribute__((packed));

/*
 * IPC CONFIG
 */


const int GPIO_PIN = 21;

bool readCommand(command_t &answer);
void flushRX();


int main(int argc, char** argv){

	setuid(0);

	/*
	 * setting up radio
     */
  	radio.begin();


   	// optionally, increase the delay between retries & # of retries
   	radio.begin();
    //radio.setPayloadSize(sizeof(command_t));
    radio.enableDynamicPayloads();
    radio.setAutoAck(0);
    //radio.setRetries(15,15);
    radio.setDataRate(RF24_250KBPS);
    radio.setPALevel(RF24_PA_MAX);
    radio.setChannel(channel);
  	// Dump the configuration of the rf unit for debugging
  	radio.printDetails();


  	// This simple sketch opens two pipes for these two nodes to communicate
  	// back and forth.
  	//I'm the sink on pipe 0
    radio.openReadingPipe(1,sinkAddress);

	/*
	 * setting up gpio
	 */
	char gpio_export_path[255];
	sprintf(gpio_export_path, "/sys/class/gpio/export");
	ofstream gpio_export;
  	gpio_export.open (gpio_export_path);
  	gpio_export << GPIO_PIN;
  	gpio_export.close();

	char gpio_direction_path[255];
	sprintf(gpio_direction_path, "/sys/class/gpio/gpio%d/direction", GPIO_PIN);
	ofstream gpio_direction;
  	gpio_direction.open (gpio_direction_path);
  	gpio_direction << "in";
  	gpio_direction.close();

	char gpio_edge_path[255];
	sprintf(gpio_edge_path, "/sys/class/gpio/gpio%d/edge", GPIO_PIN);
	ofstream gpio_edge;
  	gpio_edge.open (gpio_edge_path);
  	gpio_edge << "falling";
  	gpio_edge.close();





	radio.startListening();

	char pathToGPIO[256];
	sprintf(pathToGPIO,"/sys/class/gpio/gpio%d/value", GPIO_PIN);

	char buffer[1024];
	while(true){
		flushRX();

		
		int fd = open(pathToGPIO, O_RDONLY);
		if(fd < 0)
			perror("open");
		
		fd_set gpio;
		FD_ZERO(&gpio);
		FD_SET(fd, &gpio);
		
		timeval to;
		to.tv_sec = 10000;


		read(fd,buffer,2);
		select(fd+1, 0,0,&gpio,&to);	
		
		close(fd);

		command_t answer;
		if(readCommand(answer)){
		}
		flushRX();
		radio.stopListening();
		radio.startListening();

	}
	return 0;
}

void flushRX(){
	radio.startListening();
	while (radio.available()) {
		char dummy;
		radio.read( &dummy, sizeof(char));
	}
}

bool readCommand(command_t &answer){

	// Wait here until we get a response, or timeout (250ms)
	unsigned long started_waiting_at = millis();
	bool ok = true;
	while ( ! radio.available() && ok ) {
		if (millis() - started_waiting_at > 200 )
			ok = false;
	}

	if(ok){
		radio.read( &answer, sizeof(command_t));
		if(answer.checkCode()){	
			char tstr[100];
			time_t t;
			time(&t);
			strftime(tstr, 100, "%e.%m.%y;%T", localtime(&t));
			//printf("%s - Got response from node %d, answer=%d, state=%d, code=%d%d%d%d\n", tstr, answer.nodeid, answer.cmd, answer.state, answer.code[0], answer.code[1], answer.code[2], answer.code[3]);
			
			std::ofstream outfile;

			char buffer[256];
			sprintf(buffer, ";%d;%d\n", answer.nodeid, answer.state);

  			outfile.open ("/var/log/temperature",fstream::app );
  			outfile << tstr << buffer;
  			outfile.close();


		}
	}
	else{
		
	}


	return ok;
}


