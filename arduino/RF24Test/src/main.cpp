
/*
   Getting Started example sketch for nRF24L01+ radios
   This is a very basic example of how to send data from one node to another
Updated: Dec 2014 by TMRh20
*/

#include <SPI.h>
#include "RF24.h"
//#include "printf.h"
#include <avr/sleep.h>
#include "printf.h"

struct command_t {
  	command_t() {
    	this->nodeid = 0;
    	this->cmd = 0;
    	this->state = 0;
  	}

  	uint8_t nodeid;
  	uint8_t cmd; /* 0 set, 1 get */
  	uint16_t state;
  	uint8_t code[4];
} __attribute((packed));



//#define DEBUG

/****************** User Config ***************************/
/***      Set this radio as radio number 0 or 1         ***/


/* Hardware configuration: Set up nRF24L01 radio on SPI bus plus pins 7 & 8 */
//RF24 radio(7, 8);
RF24 radio(7, 8);
const int radioIrq = 3;
//nodes

void checkRadio();
void setup() {
 	Serial.begin(115200);


  	/* setup radio */
  	radio.begin();

  	//radio.setRetries(250, 30);
  	// Set the PA Level low to prevent power supply related issues since this is a
  	// getting_started sketch, and the likelihood of close proximity of the devices. RF24_PA_MAX is default.
  	radio.setPALevel(RF24_PA_MAX);

  	// Open a writing and reading pipe on each radio, with opposite addresses
  	radio.openWritingPipe((uint64_t)0);
  	radio.openReadingPipe(1, (uint64_t)1);

  	radio.enableDynamicPayloads();
  	radio.setAutoAck(0);
  	//radio.setRetries(15, 15);
  	radio.setDataRate(RF24_250KBPS);
  	radio.setPALevel(RF24_PA_MAX);
  	radio.setChannel(3);

  	attachInterrupt(digitalPinToInterrupt(radioIrq), checkRadio, FALLING);

 	printf_begin();
  	radio.printDetails();
  	radio.startListening();

}

void checkRadio() {
	noInterrupts();
  	bool tx, fail, rx;
  	radio.whatHappened(tx, fail, rx);                   // What happened?

	Serial.println("Interrupt:");

  	// If data is available, handle it accordingly
  	if ( rx ) {
  		Serial.println("Try read..\n");

  		command_t cmd;
    	radio.read( &cmd, sizeof(command_t));             // Get the payload
		Serial.print(" id: ");Serial.print(cmd.nodeid);
		Serial.print(" cmd: ");Serial.print(cmd.cmd);
		Serial.print(" state: ");Serial.print(cmd.state);
		Serial.println();
  	}
  	if(fail){
  		Serial.println("Radio: failed\n");
  	}
  	interrupts();
}

command_t command;
void loop() {
	noInterrupts();
	radio.stopListening();
	delay(10);
    Serial.println("Now Sending..\n");
    command.nodeid = 2;
    command.cmd = 255; //test command
    radio.write( &command, sizeof(command_t) );              // Send the final one back.
    radio.startListening();
    interrupts();

    delay(1000);


} // Loop


