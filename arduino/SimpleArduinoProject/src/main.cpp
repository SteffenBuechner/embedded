
/*
  Getting Started example sketch for nRF24L01+ radios
  This is a very basic example of how to send data from one node to another
  Updated: Dec 2014 by TMRh20
*/

#include "SPI.h"

void setup() {
  Serial.begin(9600);
}


int run = 0;
void loop() {
	Serial.print("Hello Nr:");
	Serial.println(run++);
	Serial.println("Yippieh!");
	delay(1000);
} // Loop

