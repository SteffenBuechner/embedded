/*
 * =====================================================================================
 *
 *       Filename:  RadioProtocol.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29.03.2016 12:02:55
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef RadioProtocol_h_
#define RadioProtocol_h_

#include <SPI.h>
#include "RF24.h"

struct command_t {
  	command_t() {
    	this->nodeid = 0;
    	this->cmd = 0;
    	this->state = 0;
  	}

  	uint8_t nodeid;
  	uint8_t cmd; /* 0 set, 1 get */
  	uint16_t state;
  	uint8_t code[4];
} __attribute((packed));



void checkRadio();

class RadioProtocol;
RadioProtocol *internal_radioProtocol = 0;

class RadioProtocol{
	public:
		RadioProtocol(RF24 &radio, uint8_t nodeid):
			radio(radio),
			nodeid(nodeid){
				internal_radioProtocol = this;
			}


		void setup(uint8_t channel, uint8_t interruptPin, bool listen){
			this->listen = listen;
  			radio.begin();

  			// Open a writing and reading pipe on each radio, with opposite addresses
  			radio.openWritingPipe((uint64_t)0);
  			radio.openReadingPipe(1, (uint64_t)1);

  			radio.enableDynamicPayloads();
  			radio.setAutoAck(0);
  			radio.setDataRate(RF24_250KBPS);
  			radio.setPALevel(RF24_PA_MAX);
  			radio.setChannel(channel);

			pinMode(interruptPin, INPUT);
			digitalWrite(interruptPin, HIGH);
  			attachInterrupt(digitalPinToInterrupt(interruptPin), checkRadio, FALLING);

			radio.stopListening();
			if(listen)
  				radio.startListening();
		}

		bool isNewCommandAvailable(){
			return receivedSomething == true;
		}

		command_t getCommand(){
			receivedSomething = false;
			return receivedCommand;
		}	

		void send(int8_t cmd, int16_t state, uint16_t retrans){
			noInterrupts();
			radio.stopListening();
    		command_t command;
			command.nodeid = nodeid;
    		command.cmd = cmd; //test command
			command.state = state;
    		radio.write( &command, sizeof(command_t) );              // Send the final one back.
    		interrupts();

			for(uint16_t i = 0; i < retrans; i++){
				Serial.println("Send..\n");
				
    			radio.startListening();
    			delay(100);

				noInterrupts();
				if(radio.available()){
					Serial.println("received something..");
	  				command_t cmd;
    				radio.read( &cmd, sizeof(command_t));             // Get the payload
					if(cmd.nodeid == nodeid){
						
						Serial.println("Ack received..\n");
						interrupts();
						return;
					}
				}
				radio.stopListening();
    			radio.write( &command, sizeof(command_t) );              // Send the final one back.
    			interrupts();
    			radio.startListening();
			}

			if(listen)
    			radio.startListening();
    		else
    			radio.stopListening();
		}

		void processInterrupt(){
			radio.stopListening();
  			// If data is available, handle it accordingly
  			if ( radio.available() ) {
  				command_t cmd;
    			radio.read( &cmd, sizeof(command_t));             // Get the payload
				if(cmd.nodeid == nodeid){
					receivedCommand = cmd;
					receivedSomething = true;
				}
  			}
			if(listen)
				radio.startListening();
		}

	private:
		RF24 &radio;
		uint8_t nodeid;
		command_t receivedCommand;
		bool receivedSomething;
		bool listen;
};

void checkRadio(){
	noInterrupts();
	internal_radioProtocol->processInterrupt();
	interrupts();
}


#endif
