/*
 * =====================================================================================
 *
 *       Filename:  SoilHumiditySensor.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29.03.2016 11:13:21
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef SoilHumiditySensor_h_
#define SoilHumiditySensor_h_

class SoilHumiditySensor{
	public:
		SoilHumiditySensor(uint8_t analogInput, uint8_t powerPin):
			analogInput(analogInput),
			powerPin(powerPin){

		}

		void begin(){
			pinMode(powerPin, OUTPUT);
			digitalWrite(powerPin, LOW);
		}

		uint16_t read(){
			digitalWrite(powerPin, HIGH);
			delay(20);
			uint16_t value = analogRead(analogInput);
			digitalWrite(powerPin, LOW);
			return value;
		}


	private:
		const uint8_t analogInput;
		const uint8_t powerPin;
};

#endif
