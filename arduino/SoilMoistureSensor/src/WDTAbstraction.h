/*
 * =====================================================================================
 *
 *       Filename:  WDTAbstraction.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29.03.2016 10:55:48
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef WDTAbstraction_h_
#define WDTAbstraction_h_

#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>

/* Interrupt service routine for watchdog
*/

bool wdtTriggered = false;
ISR(WDT_vect)
{
	wdtTriggered = true;
}

class WDTAbstraction{
	public:
		WDTAbstraction(){

		}

		void setup(){
    		/*** Setup the WDT ***/

  			/* Clear the reset flag. */
  			MCUSR &= ~(1<<WDRF);

  			/* In order to change WDE or the prescaler, we need to
   	 	 	 * set WDCE (This will allow updates for 4 clock cycles).
   	 	 	 */
  			WDTCSR |= (1<<WDCE) | (1<<WDE);

  			/* set new watchdog timeout prescaler value */
  			WDTCSR = 1<<WDP0 | 1<<WDP3; /* 8.0 seconds */

  			/* Enable the WD interrupt (note no reset). */
  			WDTCSR |= _BV(WDIE);
		}

		void powerAllDown(){

			/* Disable all of the unused peripherals. This will reduce power
   			 * consumption further and, more importantly, some of these
   			 * peripherals may generate interrupts that will wake our Arduino from
   			 * sleep!
   			 */
  			power_adc_disable();
  			power_spi_disable();
  //			power_timer0_disable();
  			power_timer1_disable();
  			power_timer2_disable();
  			power_twi_disable();  
		}

		void sleep(int cycles)
		{
			wdtTriggered = false;
  			while(true){
  				if(wdtTriggered) 
  					cycles--;
  				wdtTriggered = false;
  				if(cycles == 0)
  					return;

    			set_sleep_mode(SLEEP_MODE_IDLE);   /* EDIT: could also use SLEEP_MODE_PWR_DOWN for lowest power consumption. */
    			sleep_enable();

    			/* Now enter sleep mode. */
    			sleep_mode();

    			/* The program will continue from here after the WDT timeout*/
    			sleep_disable(); /* First thing to do is disable sleep. */

    			/* Re-enable the peripherals. */
    			power_all_enable();
  			}
		}
};

#endif
