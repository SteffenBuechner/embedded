/*
 * =====================================================================================
 *
 *       Filename:  WateringPump.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  30.03.2016 11:37:23
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef WateringPump_h_
#define WateringPump_h_

#include "vtimer/VirtualTimer.h"

class WateringPump : public VirtualTimer{
	public:
		enum{
			wateringTime = 1000
		};
		WateringPump(uint8_t pump_pin):
			VirtualTimer(VirtualTimer::ONE_SHOT),
			pump_pin(pump_pin){
		}

		void begin(){
			VirtualTimer::begin();
			pinMode(pump_pin, OUTPUT);
		}

		void execute(){
			Serial.println("Stop");
			stopPump();
		}
		
		void stopPump(){
			analogWrite(pump_pin, 0);
		}

		void pump(long ms){
			analogWrite(pump_pin, 100);
			VirtualTimer::start(ms);
			Serial.println("Start");
		}


	private:
		uint8_t pump_pin;
};
#endif
