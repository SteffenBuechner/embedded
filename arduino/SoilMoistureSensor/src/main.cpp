
/*
   Getting Started example sketch for nRF24L01+ radios
   This is a very basic example of how to send data from one node to another
Updated: Dec 2014 by TMRh20
*/

#include "SPI.h"
#include "WDTAbstraction.h"
#include "SoilHumiditySensor.h"
#include "RadioProtocol.h"
#include "RF24.h"
#include "WateringPump.h"
#include "vtimer/TimerBase.h"
#include "printf.h"


RF24 radio(7,8);
RadioProtocol radioProtocol(radio, 2);
WDTAbstraction wdt;
SoilHumiditySensor soilHumidity(A0, 10);
WateringPump wateringPump(5);

const int waterPinRead = 9;
const int waterPinSupply = 6;

uint16_t humidityThreshold = 600;

struct command_value_t{
	unsigned hum:10;
	unsigned nowater:1;
	unsigned sensorok:1;
	unsigned watered:1;
	unsigned reserved:3;
} __attribute__((packed));

union command_value_dummy_t{
	command_value_t value;
	uint16_t data;
};

void outOfWater();

void setup() {
	Serial.begin(115200);
	TimerBase::begin();
	wateringPump.begin();
  	wdt.setup();
  	soilHumidity.begin();
	radioProtocol.setup(3, 3, false);

	pinMode(waterPinRead, INPUT);
	digitalWrite(waterPinRead, HIGH);
	printf_begin();
}


void loop() {
	command_value_dummy_t send_data;
	uint16_t value = soilHumidity.read();

	send_data.value.hum = value;
	send_data.value.sensorok = 1;
	send_data.value.nowater = 0;

	Serial.println(digitalRead(waterPinRead));
	if(value < 200){
		send_data.value.sensorok = 0;
	};

	if(digitalRead(waterPinRead) == 1){
		send_data.value.nowater = 1;
	} else if(value < humidityThreshold && value > 100) {
		send_data.value.watered = 1;
		wateringPump.pump(10000);
	} 

	radioProtocol.send(5, send_data.data, 20);
	Serial.println(value);

	delay(100);

	wdt.sleep((2*60)/8);
} // Loop

