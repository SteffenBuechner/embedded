/*
 * =====================================================================================
 *
 *       Filename:  TimerBase.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  30.03.2016 12:23:06
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include "TimerBase.h"
#include <stdint.h>
#include "VirtualTimer.h"
#include "Arduino.h"

// Interrupt is called once a millisecond, 
SIGNAL(TIMER0_COMPA_vect) 
{
	TimerBase::checkTimer();
};


uint8_t TimerBase::numberOfRegisteredTimers = 0;
long TimerBase::lastMillis = 0;
VirtualTimer *TimerBase::registeredTimer[TimerBase::maxTimer];

void TimerBase::begin(){
	cli();
	for(int i = 0; i < TimerBase::maxTimer; i++)
		TimerBase::registeredTimer[i] = 0;	

  	// Timer0 is already used for millis() - we'll just interrupt somewhere
  	// in the middle and call the "Compare A" function below
  	OCR0A = 0xAF;
  	TIMSK0 |= _BV(OCIE0A);
	lastMillis = millis();
	sei();

	Serial.println("Timerbase::begin");
}

void TimerBase::unregisterVirtualTimer(VirtualTimer* timer){
	if(!timer->isRegistered())
		return;
	
	for(int i = 0; i < TimerBase::maxTimer; i++){
		if(TimerBase::registeredTimer[i] == timer){
			cli();
			TimerBase::registeredTimer[i] = 0;
			sei();
			timer->setRegistered(false);
		}
	}
}

void TimerBase::registerVirtualTimer(VirtualTimer* timer){
	if(timer->isRegistered())
		return;
	
	for(int i = 0; i < TimerBase::maxTimer; i++){
		if(TimerBase::registeredTimer[i] == 0){
			cli();
			TimerBase::registeredTimer[i] = timer;
			sei();
			timer->setRegistered(true);
			return;
		}
	}
}

void TimerBase::checkTimer(){
	long elapsed  = millis() - lastMillis;
	lastMillis = millis();
	for(int i = 0; i < TimerBase::maxTimer; i++){
		if(TimerBase::registeredTimer[i] != 0 && TimerBase::registeredTimer[i]->isEnabled())
		{
			TimerBase::registeredTimer[i]->update(elapsed);
		}
	}
}


