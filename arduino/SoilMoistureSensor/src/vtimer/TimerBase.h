/*
 * =====================================================================================
 *
 *       Filename:  TimerBase.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  30.03.2016 12:17:13
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef TimerBase_h_
#define TimerBase_h_

#include <avr/interrupt.h>

class VirtualTimer;

class TimerBase{
	public:
		enum{
			maxTimer = 10
		};

		static void begin();
		static void checkTimer();
		static void registerVirtualTimer(VirtualTimer* timer);
		static void unregisterVirtualTimer(VirtualTimer* timer);
	private:
		static long lastMillis;
		static uint8_t numberOfRegisteredTimers;
		static VirtualTimer *registeredTimer[10];

		TimerBase(){
		}
};


#endif
