
/*
  Getting Started example sketch for nRF24L01+ radios
  This is a very basic example of how to send data from one node to another
  Updated: Dec 2014 by TMRh20
*/

#include <SPI.h>
#include "RF24.h"
#include "printf.h"
#include <avr/sleep.h>

//#define DEBUG

/****************** User Config ***************************/
/***      Set this radio as radio number 0 or 1         ***/


/* Hardware configuration: Set up nRF24L01 radio on SPI bus plus pins 7 & 8 */
RF24 radio(9, 10);

//nodes
const int codeLength = 4;
const int nodeId = 0;
const uint8_t commCode[codeLength] = {0, 1, 2, 3};

const int hashSize = 10;
const char MD5Salt[] = "g/678h,nGi";
const byte sinkAddress[6] = {"Sink "};
const byte nodePipe[6] = {"Nodes"};
const int radioIRQ = 2;
const int channel = 3;

const uint8_t ON = 1;
const uint8_t OFF = 0;

/*relais config*/
const int relaisPin = 8;
uint8_t lightState = 0;


/* button config */
const int buttonIRQ = 3;

const uint8_t RELAIS_STATE_SET = 1;
const uint8_t RELAIS_STATE_REQUEST = 2;
const uint8_t TEMPERATURE_REQUEST = 3;

/* communication struct */
struct command_t {
  command_t() {
    this->nodeid = nodeId;
    this->cmd = 0;
    this->state = 0;
    this->cmdNr = 0;
    memcpy(this->code, commCode, codeLength);
  }
  command_t(uint8_t cmd, uint16_t state, uint16_t cmdNr) {
    this->nodeid = nodeId;
    this->cmd = cmd;
    this->state = state;
    this->cmdNr = cmdNr;
    memcpy(this->code, commCode, codeLength);
  }

  command_t(command_t &cmd){
    this->nodeid = nodeId;
    this->cmd = cmd.cmd;
    this->state = cmd.state;
    this->cmdNr = cmd.cmdNr;
    memcpy(this->code, commCode, codeLength);
  
  }
  
  bool check() {
    bool ok = this->nodeid == nodeId;

    for (int i = 0; i < codeLength && ok; i++)
      ok &= this->code[i] == commCode[i];

    return ok;
  }

  uint8_t nodeid;
  uint8_t cmd; /* 0 set, 1 get */
  uint16_t state;
  uint8_t code[4];
  uint16_t cmdNr;
  
} __attribute((packed));

void checkRadio();
void toggleRelais();
void temperatureRequest(command_t &cmd);
uint8_t getlightState();
void setlightState(uint8_t newState);
void lightStateSet(command_t &cmd);
void lightStateRequest(command_t &cmd);

void setup() {
  /* setup button */
  pinMode(relaisPin, OUTPUT);
  digitalWrite(relaisPin, HIGH);
  lightState = OFF;
  pinMode(buttonIRQ, INPUT);
  digitalWrite(buttonIRQ, HIGH);
  attachInterrupt(digitalPinToInterrupt(buttonIRQ), toggleRelais, LOW);

  /* setup relais */

  /* setup radio */
  radio.begin();

  //radio.setRetries(250, 30);
  // Set the PA Level low to prevent power supply related issues since this is a
  // getting_started sketch, and the likelihood of close proximity of the devices. RF24_PA_MAX is default.
  radio.setPALevel(RF24_PA_MAX);

  // Open a writing and reading pipe on each radio, with opposite addresses
  radio.openWritingPipe(sinkAddress);
  radio.openReadingPipe(1, nodePipe);

  radio.begin();
  radio.enableDynamicPayloads();
  radio.setAutoAck(0);
  //radio.setRetries(15, 15);
  radio.setDataRate(RF24_250KBPS);
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(channel);


  // Start the radio listening for data
  radio.startListening();

  attachInterrupt(digitalPinToInterrupt(radioIRQ), checkRadio, LOW);

#ifdef DEBUG
  Serial.begin(9600);
  printf_begin();
  radio.printDetails();
#endif

  //good night
  set_sleep_mode(SLEEP_MODE_STANDBY);
  sleep_mode();


}

void checkRadio() {
  bool tx, fail, rx;
  radio.whatHappened(tx, fail, rx);                   // What happened?


  // If data is available, handle it accordingly
  if ( rx ) {
    command_t cmd;
    radio.read( &cmd, sizeof(command_t));             // Get the payload


#ifdef DEBUG
    Serial.println(cmd.nodeid);
    Serial.println(cmd.cmd);
    Serial.println(cmd.state);
    Serial.print(cmd.code[0]); Serial.print(cmd.code[1]); Serial.print(cmd.code[2]); Serial.println(cmd.code[3]);
#endif

    if (cmd.check()) {
      switch (cmd.cmd) {
        case RELAIS_STATE_REQUEST:
          lightStateRequest(cmd);
          break;
        case RELAIS_STATE_SET:
          lightStateSet(cmd);
          break;
        case TEMPERATURE_REQUEST:
          temperatureRequest(cmd);
          break;
        default:
        break;
      }
    }
  }
}


const unsigned long intWait = 100;
unsigned long startInt = 0;

void toggleRelais() {

  if (millis() - startInt < intWait)
    return;

  startInt = millis();

  int localLightState = getlightState();

  if (localLightState == LOW)
    localLightState = HIGH;
  else
    localLightState = LOW;

  setlightState(localLightState);


}

uint8_t getlightState() {
  return lightState;
}

void setlightState(uint8_t newState) {
  if(newState == lightState){
    #ifdef DEBUG
      Serial.println("Light is already in this state");
    #endif
    return;
  }
  digitalWrite(relaisPin, LOW);
  delay(2000);
  digitalWrite(relaisPin, HIGH);

  lightState = newState;

#ifdef DEBUG
  if (lightState == ON)
    Serial.println("Light is On!");
  else
    Serial.println("Light is Off!");
#endif
}

void loop() {

} // Loop

void lightStateSet(command_t &cmd) {
#ifdef DEBUG
  Serial.println("Relais Set");
#endif
  
  setlightState(cmd.state);

  command_t answer = cmd;
  answer.state = getlightState();

  radio.stopListening();                                        // First, stop listening so we can talk
  radio.write( &answer, sizeof(command_t) );              // Send the final one back.
  radio.startListening();
}

void lightStateRequest(command_t &cmd) {
#ifdef DEBUG
  Serial.print("Relais Request");
#endif
  uint8_t curState = getlightState();

  command_t answer(cmd);
  answer.state = curState;

  radio.stopListening();                                        // First, stop listening so we can talk
  radio.write( &answer, sizeof(command_t) );              // Send the final one back.
  radio.startListening();                                       // Now, resume listening so we catch the next packets.

}

void temperatureRequest(command_t &cmd) {
#ifdef DEBUG
  Serial.println("Temp Request");
#endif
  uint16_t curTemp = 0x4711;

  command_t answer(cmd);
  answer.state = curTemp;

  radio.stopListening();                                        // First, stop listening so we can talk
  radio.write( &answer, sizeof(command_t) );              // Send the final one back.
  radio.startListening();                                       // Now, resume listening so we catch the next packets.

}

