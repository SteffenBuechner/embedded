#!/bin/bash
for i in {2..255}; do
	#echo nslookup 193.168.2.${i} 192.168.2.1
	result=$(nslookup 192.168.2.${i} 192.168.2.1)
	if [[ ${result} = *"android"* ]]; then
		ping -c 1 192.168.2.${i}
	fi
done;
