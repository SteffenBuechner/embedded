
/*
   Getting Started example sketch for nRF24L01+ radios
   This is a very basic example of how to send data from one node to another
Updated: Dec 2014 by TMRh20
*/

#include <SPI.h>
#include "RF24.h"
//#include "printf.h"
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>

#include <DallasTemperature.h>

#define ONE_WIRE_BUS_1 8

OneWire oneWire_in(ONE_WIRE_BUS_1);

DallasTemperature sensor(&oneWire_in);


#define DEBUG

/****************** User Config ***************************/
/***      Set this radio as radio number 0 or 1         ***/


/* Hardware configuration: Set up nRF24L01 radio on SPI bus plus pins 7 & 8 */
RF24 radio(7, 8);
//RF24 radio(9, 10);

//nodes
const int codeLength = 4;
const uint8_t commCode[codeLength] = {0, 1, 2, 3};
const int NODEID = 5;

const uint64_t sinkAddress = 0;
const uint64_t nodesAddress = 1;
const int radioIRQ = 2;
const int channel = 3;

const uint8_t TEMPERATURE_REQUEST = 3;

const int dutyCycle = (5*60) / 8;
int sleepCycle = 0;

/* communication struct */
struct command_t {
  	command_t() {
    	this->nodeid = NODEID;
    	this->cmd = 0;
    	this->state = 0;
    	memcpy(this->code, commCode, codeLength);
  	}

  	uint8_t nodeid;
  	uint8_t cmd; /* 0 set, 1 get */
  	uint16_t state;
  	uint8_t code[4];
} __attribute((packed));


void temperatureRequest(command_t &cmd);


void setupWDT(){
    /*** Setup the WDT ***/

  	/* Clear the reset flag. */
  	MCUSR &= ~(1<<WDRF);

  	/* In order to change WDE or the prescaler, we need to
   	 * set WDCE (This will allow updates for 4 clock cycles).
   	 */
  	WDTCSR |= (1<<WDCE) | (1<<WDE);

  	/* set new watchdog timeout prescaler value */
  	WDTCSR = 1<<WDP0 | 1<<WDP3; /* 8.0 seconds */

  	/* Enable the WD interrupt (note no reset). */
  	WDTCSR |= _BV(WDIE);
}

void enterSleep(void)
{
  	sleepCycle = dutyCycle;
  	while(sleepCycle-- != 0){
    	set_sleep_mode(SLEEP_MODE_PWR_DOWN);   /* EDIT: could also use SLEEP_MODE_PWR_DOWN for lowest power consumption. */
    	sleep_enable();

    	/* Now enter sleep mode. */
    	sleep_mode();

    	/* The program will continue from here after the WDT timeout*/
    	sleep_disable(); /* First thing to do is disable sleep. */

    	/* Re-enable the peripherals. */
    	power_all_enable();
  	}
}

/* Interrupt service routine for watchdog
*/
ISR(WDT_vect)
{

}

void setup() {

  	/* setup radio */
  	radio.begin();

  	//radio.setRetries(250, 30);
  	// Set the PA Level low to prevent power supply related issues since this is a
  	// getting_started sketch, and the likelihood of close proximity of the devices. RF24_PA_MAX is default.
  	radio.setPALevel(RF24_PA_MAX);

  	// Open a writing and reading pipe on each radio, with opposite addresses
  	radio.openWritingPipe(sinkAddress);
  	radio.openReadingPipe(1, nodesAddress);

  	radio.enableDynamicPayloads();
  	radio.setAutoAck(0);
  	//radio.setRetries(15, 15);
  	radio.setDataRate(RF24_250KBPS);
  	radio.setPALevel(RF24_PA_MAX);
  	radio.setChannel(channel);

  	setupWDT();

  	sensor.begin();


#ifdef DEBUG
 	Serial.begin(115200);
#endif

  	radio.printDetails();

}

void loop() {
  	command_t cmd;
  	cmd.cmd = TEMPERATURE_REQUEST;
  	temperatureRequest(cmd);

  	enterSleep();
} // Loop


void temperatureRequest(command_t &cmd) {
#ifdef DEBUG
  	Serial.println("Temp Request");
#endif
  	sensor.requestTemperatures();
  	uint16_t temperature = sensor.getTempCByIndex(0) * 100;

  	command_t answer(cmd);
  	answer.state = temperature;

  	radio.powerUp();
  	delay(100);

  	for(int i = 0; i < 20; i++){
    	radio.stopListening();
    	delay(20);
    	Serial.println("Now Sending..\n");
    	radio.write( &answer, sizeof(command_t) );              // Send the final one back.
    	radio.startListening();

    	unsigned long startInt = millis();

    	while(millis() - startInt < 400){
			delay(10);
      		if(radio.available()){
#ifdef DEBUG
      	  		Serial.println("Got something");
#endif
        		command_t cmd;
        		radio.read( &cmd, sizeof(command_t));             // Get the payload
        		if(cmd.nodeid == NODEID){
          			radio.stopListening();                                       // Now, resume listening so we catch the next packets.
          			radio.powerDown();
          			delay(10);
#ifdef DEBUG
          			Serial.println("Send complete..");
#endif
					delay(10);
          			return;
        		}

      		}
    	}
    	delay(400);
  	}
  	radio.stopListening();                                       // Now, resume listening so we catch the next packets.
  	radio.powerDown();
  	delay(10);
}

