#include <ESP8266WiFi.h>
#include <WiFiServer.h>
#include "../../libraries/WiFiConfig.h"
#include "../../libraries/PushServer.h"

#include <DallasTemperature.h>

#define ONE_WIRE_BUS_1 2

OneWire oneWire_in(ONE_WIRE_BUS_1);

DallasTemperature sensor(&oneWire_in);


extern "C" {
#include "user_interface.h"
}

const char* const SSID = "IoT ESP Config";
char hostName[32] = "ESP01_Templogger";

WiFiServer server(80);

uint8_t configButtonPin = 0;
bool configMode = false;


/*
 * The config class, the SSID is for accesspoint mode.
 * See in line 63.
 */
WiFiConfig wiFiConfig(SSID);
PushServer pushServer("Temperature", "/temp.service");


/*
 * The microcontroller setup.
 */
void setup() {
  	Serial.begin(115200);

  	sensor.begin();
  	/*
   	 * Try to configure the wifi network.
   	 * If there is already a configuration,
   	 * it will be loaded otherwise the ESP will configure itself 
   	 * as an Accesspoint with the configured SSID.
   	 */
  	pushServer.begin();

  	/*
   	 * This is the webserver which is started by us
   	 * now.
   	 */
  	if(!wiFiConfig.begin()){
  		configMode = true;
  	}else{

  		Serial.println(WiFi.localIP());

		pinMode(configButtonPin, INPUT);
		digitalWrite(configButtonPin, HIGH);
  		if(digitalRead(configButtonPin) == 0){
  	  		//then config mode (no deep sleep)
  			configMode = true;
  		}else{
  			configMode = false;
  		}
  	}
  	server.begin();
}

void loop() {
	if(!configMode){
  		sensor.requestTemperatures();
  		uint16_t temperature = sensor.getTempCByIndex(0) * 100;

  		WiFiClient pushClient;
  		if(pushServer.beginPush(pushClient, wiFiConfig.getNodeID())){
  			pushServer.addPush("temp", temperature);
  			pushServer.finishPush();
  		}

		system_deep_sleep(1000000 * 60 * 5);
		delay(500);
		return;
	}

	/*
	 * Is there any client which had a request?
	 * A client is basically everything which accesses 
	 * out web server.
	 */
	WiFiClient client = server.available();
	if (!client) {
		/*
 	 	 * If there is no client we will just
 	 	 * return.
 	 	 */
  	  	return;
	}

	/*
	 * Here we are only interested in the first line of the request.
	 */
	GetRequest request(client.readStringUntil('\r')); //read only get request (1.line)

	/*
	 * Well print the line to the serial port for debugging.
	 */
	Serial.println(request.getRequestString());

	/*
	 * If the request is a wifi handle request
	 * (http://server/networkConfigDialog)
	 * then we will show the corresponding config page
	 * see WiFiConfig.h, otherwise we see what
	 * the client wants, e.g., show the state of the 
	 * relais.
	 */

	Webpage page(client);
	page.beginPage();

	if(wiFiConfig.handleRequest(request, page)){
	}else if(pushServer.handleRequest(request, page)){
	}else{
		page.add("<h1>The available links are.</h1>");
		pushServer.addSetupLink(page);
		page.addBreak();
		wiFiConfig.addSetupLink(page);
 	}    

 	page.finishPage();
}


