
/*
 * Getting Started example sketch for nRF24L01+ radios
 * This is a very basic example of how to send data from one node to another
 * Updated: Dec 2014 by TMRh20
 */

#include <SPI.h>
#include <ESP8266WiFi.h>
#include <WiFiServer.h>
#include "../../libraries/WiFiConfig.h"
#include "../../libraries/Webpage.h"
#include "../../libraries/NTPClient.h"
#include "../../libraries/Timer.h"
#include "../../libraries/Clock.h"

const char* const SSID = "IoT ESP Config";

WiFiServer server(80);
WiFiConfig wiFiConfig(SSID);
Clock clock(1978);

void setup() {
  	Serial.begin(115200);

  	wiFiConfig.begin();
  	server.begin();
  	Serial.println(WiFi.localIP());
	clock.begin();
}

void loop() {
	/*
	 * Is there any client which had a request?
	 * A client is basically everything which accesses 
	 * out web server.
	 */
	WiFiClient client = server.available();
	if (!client) {
		/*
 	 	 * If there is no client we will just
 	 	 * return.
 	 	 */
  	  	return;
	}

	/*
	 * Here we are only interested in the first line of the request.
	 */
	GetRequest request(client); //read only get request (1.line)

	/*
	 * Well print the line to the serial port for debugging.
	 */
	Serial.println(request.getRequestString());

	/*
	 * If the request is a wifi handle request
	 * (http://server/networkConfigDialog)
	 * then we will show the corresponding config page
	 * see WiFiConfig.h, otherwise we see what
	 * the client wants, e.g., show the state of the 
	 * relais.
	 */

	Webpage page(client);
	page.beginPage();

	if(wiFiConfig.handleRequest(request, page)){
	}else{
		page.addHeadline("Current timestamp:");
		page.add(clock.getTime());
		page.addHeadline("The available links are.");
		wiFiConfig.addSetupLink(page);

 	}    

 	page.finishPage();

}
