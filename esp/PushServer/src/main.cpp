#include <ESP8266WiFi.h>
#include <WiFiServer.h>
#include "../../libraries/WiFiConfig.h"
#include "../../libraries/PushServer.h"

const char* const SSID = "IoT ESP Config";

WiFiServer server(80);

WiFiConfig wiFiConfig(SSID);
const char * const serviceName = "Test";
const char * const serviceConfigName = "/Test.config";
PushServer pushServer(serviceName, serviceConfigName);


/*
 * The microcontroller setup.
 */
void setup() {
  	Serial.begin(115200);

  	/*
   	 * Try to configure the wifi network.
   	 * If there is already a configuration,
   	 * it will be loaded otherwise the ESP will configure itself 
   	 * as an Accesspoint with the configured SSID.
   	 */
  	wiFiConfig.begin();
  	pushServer.begin();

  	/*
   	 * This is the webserver which is started by us
   	 * now.
   	 */
  	server.begin();

  	Serial.println(WiFi.localIP());
}

void loop() {
	/*
	 * Is there any client which had a request?
	 * A client is basically everything which accesses 
	 * out web server.
	 */
	WiFiClient client = server.available();
	if (!client) {
		/*
 	 	 * If there is no client we will just
 	 	 * return.
 	 	 */
  	  	return;
	}

	/*
	 * Here we are only interested in the first line of the request.
	 */
	GetRequest request(client); //read only get request (1.line)

	/*
	 * Well print the line to the serial port for debugging.
	 */
	Serial.println(request.getRequestString());

	/*
	 * If the request is a wifi handle request
	 * (http://server/networkConfigDialog)
	 * then we will show the corresponding config page
	 * see WiFiConfig.h, otherwise we see what
	 * the client wants, e.g., show the state of the 
	 * relais.
	 */

	Webpage page(client);
	page.beginPage();

	if(wiFiConfig.handleRequest(request, page)){
	}else if(pushServer.handleRequest(request, page)){
	}else{
		if(request.isRequestFor("showPushServerOutput")){
			WiFiClient pc;
			if(pushServer.beginPush(pc, wiFiConfig.getNodeID())){
				pushServer.addPush("field1", "hI");
				pushServer.addPush("field2", 12);
				pushServer.finishPush();

				page.add("Push done");
			}else{
				page.add(client, "Something went wrong.");
			}
		}
		else{
			page.add("<h1>The available links are.</h1>");
			pushServer.addSetupLink(page);
			page.addBreak();
			wiFiConfig.addSetupLink(page);
  	  	}
 	}    

 	page.finishPage();
}


