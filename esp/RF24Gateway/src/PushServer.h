#ifndef PushServer_h_
#define PushServer_h_

#include <ESP8266WiFi.h>
#include <WiFiServer.h>
#include <FS.h>
#include "Webpage.h"
//#define DEBUG_CONF_TOOL

class PushServer{
	private:

  		const char* const HTML_HEADER = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><body>";
  		const char* const HTML_FOOTER = "</html></body>";
  		const char* const HTML_HEADLINE = "<h1>ServerIP config screen</h1>"
            "Please enter the IP of the server (no special characters allowed).</br>";

  		const char* const HTML_SETUP_FORM_BEGIN = "<form action=\"setupServer\" method=\"get\">";
  		const char* const HTML_SETUP_FORM_SERVER_BEGIN = "Server IP:<br> <input type=\"text\" name=\"serverip\" value=\"";
  		const char* const HTML_SETUP_FORM_SERVER_END = "\"> <br>";
  		const char* const HTML_SETUP_FORM_END = "<form action=\"setupServer\" method=\"get\"><input type=\"Submit\" value=\"Setup\"></form>";
        const char* const SETUP_LINK = "<a href=\"serverSetupDialog\">Server Setup</a>";

  		// Create an instance of the server
  		// specify the port to listen on as an argument

  		char serverIP[16];

		void resetConfig(){
			memcpy(serverIP, "128.0.0.1", 10) ;
		}

		bool loadConfig() {
  			if(!SPIFFS.begin()){
  				resetConfig();
				return false;
			}

  			File configFile = SPIFFS.open("/config.server", "r");
  			if (!configFile) {
#ifdef DEBUG_CONF_TOOL
				Serial.println("Failed to open config file");
#endif
				resetConfig();
    			return false;
  			}

  			size_t size = configFile.size();
  			if (size > 20) {
				resetConfig();
    			return false;
  			}

  			char content[size];
  			configFile.readBytes(serverIP, size);

  			int len = strlen(content);
  			if(len > 15){
  				resetConfig();
				return false;
			}

#ifdef DEBUG_CONF_TOOL
  			Serial.println(serverip);
#endif
  			return true;
		}

		bool saveConfig(const char *sIP) {
  			if(!SPIFFS.begin())
				return false;
  			File configFile = SPIFFS.open("/config.server", "w");
  			if (!configFile) {
			#ifdef DEBUG_CONF_TOOL
    			Serial.println("Failed to open config file for writing");
			#endif
    			return false;
  			}

  			char *content = new char[strlen(sIP)+1]; //+1 for newline and 0
  			sprintf(content, "%s", sIP);
  			configFile.write((uint8_t*)content, strlen(content)+1);

  			configFile.close();
  			return true;
		}

	public:
  		PushServer(){
  			setupServer();
  	  	}

  	  	const char* getSetupLink(){
  	  		return SETUP_LINK;
  	  	}

  		bool handleConfigAnswer(String &request, WiFiClient &client){
			if(request.indexOf("setupServer") != -1){
      			int posServerIP = request.indexOf("serverip=");
      			int endServerIp = request.indexOf(" ", posServerIP);

      			bool isSetupString = (posServerIP != -1);
      			if(isSetupString){
        			String dummy = request.substring(posServerIP + 9, endServerIp);
        			Serial.println(dummy);
					
					#ifdef DEBUG_CONF_TOOL
        			Serial.println(dummy);
					#endif

					client.flush();
					client.println("Configuration updated. It will be set in 15s.");
					client.stop();

					delay(15000);

					return saveConfig(dummy.c_str()); 
      			}
      			else
      				showConfig(client);
    		}
			return false;
  		}

  		bool isConfigAnswer(String &request){
			return request.indexOf("setupServer") != -1;
  		}
  		bool isConfigRequest(String &request){
			return request.indexOf("serverSetupDialog") != -1;
  		}

  		void showConfig(WiFiClient &client){
     		client.flush(); //clear previous info in the stream 
			Webpage::beginPage(client);
			Webpage::add(client,HTML_HEADLINE);
			Webpage::add(client,HTML_SETUP_FORM_BEGIN);
			Webpage::add(client,HTML_SETUP_FORM_SERVER_BEGIN);
#if DEBUG_CONF_TOOL 
			Serial.println(serverIP);
#endif
			Webpage::add(client,serverIP);
			Webpage::add(client,HTML_SETUP_FORM_SERVER_END);
			Webpage::add(client,HTML_SETUP_FORM_END);

			Webpage::finishPage(client);
  		}

  		bool handleRequest(String &request, WiFiClient &client){
			if(isConfigRequest(request)){
				showConfig(client);
				return true;	
			}else if(isConfigAnswer(request)){
				if(handleConfigAnswer(request, client)){
					setupServer();
				}
				return true;
			}

			return false;
  		}

  		void setupServer(){
			loadConfig();
     	}
};

#endif
