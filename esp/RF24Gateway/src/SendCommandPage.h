#ifndef SendCommandPage_h_
#define SendCommandPage_h_

#include <ESP8266WiFi.h>
#include <WiFiServer.h>
#include <FS.h>
#include <RF24.h>
#include "command_t.h"

//#define DEBUG_CONF_TOOL

class SendCommandPage{
	private:

  		const char* const HTML_HEADER = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><body>";
  		const char* const HTML_FOOTER = "</html></body>";
  		const char* const HTML_COMMAND_HEADLINE = "<h1>New Command Screen</h1>"
            "Please enter the nodidm the command code and the state for the command..</br>";

  		const char* const HTML_COMMAND_SETUP_FORM = "<form action=\"sendCommand\" method=\"get\">"
            "NODEID:<br> <input type=\"number\" name=\"NODEID\"/> <br>"
            "COMMAND: <br><input type=\"number\" name=\"COMMAND\"/><br>"
            "STATE: <br><input type=\"number\" name=\"STATE\"/><br><br>"
            "<input type=\"Submit\" value=\"Setup\">"
            "</form>";

        const char* const SETUP_LINK = "<a href=\"newCommand\">Send a new command</a>";

  		// Create an instance of the server
  		// specify the port to listen on as an argument

	public:
  		SendCommandPage(){}

  	  	const char* getSetupLink(){
  	  		return SETUP_LINK;
  	  	}

  	public:

  		bool handleConfigAnswer(String &request, WiFiClient &client, RF24 &radio){
			if(request.indexOf("sendCommand") != -1){
      			int posNODEID = request.indexOf("NODEID=");
      			int endNODEID = request.indexOf("&", posNODEID);
      			int posCMD = request.indexOf("COMMAND=");
      			int endCMD = request.indexOf("&", posCMD);
      			int posSTATE = request.indexOf("STATE=");
      			int endSTATE = request.indexOf(" ", posSTATE);


      			bool isSetupString = (posNODEID != -1) && (posCMD != 0) && (posSTATE != 0);
      			if(isSetupString){
        			String nodeid_s = request.substring(posNODEID + 7, endNODEID);
        			String cmd_s = request.substring(posCMD + 8, endCMD);
        			String state_s = request.substring(posSTATE + 6, endSTATE);

					uint8_t nodeid = atoi(nodeid_s.c_str());
					uint8_t cmd = atoi(cmd_s.c_str());
					uint16_t state = atoi(state_s.c_str());
        			Serial.println(nodeid);
        			Serial.println(cmd);
        			Serial.println(state);

					command_t command;
					command.nodeid = nodeid;
					command.cmd = cmd;
					command.state = state;

					noInterrupts();
					radio.stopListening();
					radio.write(&command, sizeof(command_t));
					radio.startListening();
					interrupts();

					client.flush();
					client.print(HTML_HEADER);
					client.println("Command Send.\n");
					client.println(getSetupLink());
					client.print(HTML_FOOTER);
					client.stop();

      			}
    		}
			return false;
  		}

  		

  		bool isSendCommandRequest(String &request){
			return request.indexOf("sendCommand") != -1;
  		}
  		bool isNewCommandRequest(String &request){
			return request.indexOf("newCommand") != -1;
  		}

  		void showPage(WiFiClient &client){
     		client.flush(); //clear previous info in the stream 
     		client.print(HTML_HEADER);
     		client.print(HTML_COMMAND_HEADLINE);
     		client.print(HTML_COMMAND_SETUP_FORM);

     		client.print(HTML_FOOTER);
  		}

  		bool handleRequest(String &request, WiFiClient &client, RF24 &radio){
			if(isNewCommandRequest(request)){
				showPage(client);
				return true;	
			}else if(isSendCommandRequest(request)){
				if(handleConfigAnswer(request, client, radio)){
				}
				return true;
			}

			return false;
  		}
};

#endif
