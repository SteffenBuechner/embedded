/*
 * =====================================================================================
 *
 *       Filename:  SimpleJSONWebpage.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  20.03.2016 09:38:16
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef SimpleJSONWebpage_h_
#define SimpleJSONWebpage_h_

#include <WiFiServer.h>

class SimpleJSONWebpage{
	static constexpr const char* const JSON_HEADER = "HTTP/1.1 200 OK\n"
      													"Content-Type: application/json\n"
      													"Access-Control-Allow-Origin: *\n"
      													"Connection: close\n\n";


  	static constexpr const char* const HTML_FOOTER = "</html></body>";

 
	public:
  		static void beginPage(WiFiClient &client){
  			client.print(SimpleJSONWebpage::JSON_HEADER);
  		}

  		static void finishPage(WiFiClient &client){
  			client.flush();
  		}

  		static void add(WiFiClient &client, const char* content){
  			client.print(content);
  		}
};

#endif
