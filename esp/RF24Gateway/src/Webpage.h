/*
 * =====================================================================================
 *
 *       Filename:  Webpage.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  20.03.2016 09:38:16
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef Webpage_h_
#define Webpage_h_

#include <WiFiServer.h>

class Webpage{
	static constexpr const char* const HTML_HEADER = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><body>";
  	static constexpr const char* const HTML_FOOTER = "</html></body>";
 
	public:

  		static void beginPage(WiFiClient &client){
  			client.print(Webpage::HTML_HEADER);
  		}

  		static void finishPage(WiFiClient &client){
  			client.print(Webpage::HTML_FOOTER);
  			client.flush();
  		}

  		static void add(WiFiClient &client, const char* content){
  			client.print(content);
  		}
};

#endif
