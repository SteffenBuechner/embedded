#ifndef WiFiConfig_h_
#define WiFiConfig_h_

#include <ESP8266WiFi.h>
#include <WiFiServer.h>
#include <FS.h>

//#define DEBUG_CONF_TOOL

class WiFiConfig{
	private:

  		const char* const HTML_HEADER = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><body>";
  		const char* const HTML_FOOTER = "</html></body>";
  		const char* const HTML_WIFI_HEADLINE = "<h1>IoT device config screen</h1>"
            "Please enter ssid and password (no special characters allowed).</br>";

  		const char* const HTML_WIFI_SETUP_FORM = "<form action=\"setupNetwork\" method=\"get\">"
            "SSID:<br> <input type=\"text\" name=\"SSID\"/> <br>"
            "PSK: <br><input type=\"text\" name=\"PSK\"/><br><br>"
            "<input type=\"Submit\" value=\"Setup\">"
            "</form>";

        const char* const SETUP_LINK = "<a href=\"networkConfigDialog\">Network Setup</a>";

  		// Create an instance of the server
  		// specify the port to listen on as an argument

  		const char *ssid;
  		char wiFiSSID[20];
  		char wiFiPSK[20];

		bool loadConfig() {
  			if(!SPIFFS.begin())
				return false;

  			File configFile = SPIFFS.open("/config.wifi", "r");
  			if (!configFile) {
#ifdef DEBUG_CONF_TOOL
				Serial.println("Failed to open config file");
#endif
    			return false;
  			}

  			size_t size = configFile.size();
  			if (size > 1024) {
    			return false;
  			}

  			char content[size];
  			configFile.readBytes(content, size);

  			int len = strlen(content);
  			int split = -1;
  			for(int i = 0; i < len; i++){
				if(content[i] == '\n'){
					split = i;
					break;
				}
  			}

  			if(split == -1)
				return false;

  			memcpy(wiFiSSID, content, split);
  			wiFiSSID[split] = 0;
  			memcpy(wiFiPSK, &content[split + 1], len - 1 - split);
  			wiFiPSK[len-1-split] = 0;


#ifdef DEBUG_CONF_TOOL
  			Serial.println(wiFiSSID);
  			Serial.println(wiFiPSK);
#endif
  			return true;
		}

		bool saveConfig(const char *ssid, const char *psk) {
  			if(!SPIFFS.begin())
				return false;
  			File configFile = SPIFFS.open("/config.wifi", "w");
  			if (!configFile) {
			#ifdef DEBUG_CONF_TOOL
    			Serial.println("Failed to open config file for writing");
			#endif
    			return false;
  			}

  			char *content = new char[strlen(ssid) + strlen(psk) + 2]; //+1 for newline and 0
  			sprintf(content, "%s\n%s", ssid, psk);
  			configFile.write((uint8_t*)content, strlen(content)+1);

  			configFile.close();
  			return true;
		}

	public:
  		WiFiConfig(const char *ssid):
  	  		ssid(ssid){
    			wiFiSSID[0] = 0;
				wiFiPSK[0]=0;
  			}

  	  	const char* getSetupLink(){
  	  		return SETUP_LINK;
  	  	}

  		/*
   		 * Try to connect to WiFi
   		 * if not succeeding go to AP mode
   		 */
  		bool connectToWiFiNetwork(){
    		WiFi.mode(WIFI_STA); 
    		WiFi.begin(wiFiSSID, wiFiPSK);

    		char retries = 25;
    		while ((WiFi.status() != WL_CONNECTED) && ((--retries) != 0)) {
      			delay(500);
    		}
    		return WiFi.status() == WL_CONNECTED;
  		}

  	public:

  		bool handleConfigAnswer(String &request, WiFiClient &client){
			if(request.indexOf("setup") != -1){
      			int posSSID = request.indexOf("SSID=");
      			int endSSID = request.indexOf("&", posSSID);
      			int posPSK = request.indexOf("PSK=");
      			int endPSK = request.indexOf(" ", posPSK);

      			bool isSetupString = (posSSID != -1) && (endSSID != -1) && (posPSK != -1) && (endPSK != -1);
      			if(isSetupString){
        			String ssid = request.substring(posSSID + 5, endSSID);
        			String psk = request.substring(posPSK + 4, endPSK);
					
					#ifdef DEBUG_CONF_TOOL
        			Serial.println(ssid);
        			Serial.println(psk);
					#endif

					client.flush();
					client.println("Configuration updated. It will be set in 15s.");
					client.stop();

					delay(15000);

					return saveConfig(ssid.c_str(), psk.c_str()); 
      			}
    		}
			return false;
  		}

  		bool isConfigAnswer(String &request){
			return request.indexOf("setupNetwork") != -1;
  		}
  		bool isConfigRequest(String &request){
			return request.indexOf("networkConfigDialog") != -1;
  		}

  		void showConfig(WiFiClient &client){
     		client.flush(); //clear previous info in the stream 
     		client.print(HTML_HEADER);
     		client.print(HTML_WIFI_HEADLINE);
     		client.print(HTML_WIFI_SETUP_FORM);

     		client.print(HTML_FOOTER);
  		}

  		bool handleRequest(String &request, WiFiClient &client){
			if(isConfigRequest(request)){
				showConfig(client);
				return true;	
			}else if(isConfigAnswer(request)){
				if(handleConfigAnswer(request, client)){
					setupWiFi();
				}
				return true;
			}

			return false;
  		}

  		void setupWiFi(){
			loadConfig();
      		// Connect to WiFi network
    		if(!connectToWiFiNetwork()){
	  			WiFi.mode(WIFI_AP_STA);
      			WiFi.softAP(ssid);
				#ifdef DEBUG_CONF_TOOL
      			Serial.print("AP mode IP:");
      			Serial.println(WiFi.softAPIP());
				#endif
			}
  		}
};

#endif
