/*
 * =====================================================================================
 *
 *       Filename:  command_t.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29.03.2016 15:09:13
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef command_t_h_
#define command_t_h_
/* communication struct */
struct command_t {
  	command_t() {
    	this->nodeid = 0;
    	this->cmd = 0;
    	this->state = 0;
  	}

  	uint8_t nodeid;
  	uint8_t cmd; /* 0 set, 1 get */
  	uint16_t state;
  	uint8_t code[4];
} __attribute((packed));  




#endif
