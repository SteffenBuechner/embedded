
/*
 * Getting Started example sketch for nRF24L01+ radios
 * This is a very basic example of how to send data from one node to another
 * Updated: Dec 2014 by TMRh20
 */

#include <SPI.h>
#include <RF24.h>
#include <ESP8266WiFi.h>
#include <WiFiServer.h>
#include "WiFiConfig.h"
#include "Webpage.h"
#include "SimpleJSONWebpage.h"
#include "NTPClient.h"
#include "Timer.h"
#include "Clock.h"
#include "SendCommandPage.h"
#include "command_t.h"
#include <DallasTemperature.h>

const int temperaturPin = 5;
OneWire oneWire_Temperature(temperaturPin);
DallasTemperature temperatureSensor(&oneWire_Temperature);


const char* const SSID = "IoT ESP Config";

WiFiServer server(80);
WiFiConfig wiFiConfig(SSID);
SendCommandPage sendCommandPage;
Clock clock(1978);

/* Hardware configuration: Set up nRF24L01 radio on SPI bus plus pins 7 & 8 */
RF24 radio(0,15);
/**********************************************************/
const int radioIRQ = 2;
const int channel = 3;
uint64_t sinkAddress = 0;
uint64_t nodesAddress = 1;

// Used to control whether this node is sending or receiving

//command structs for each nodeid
const int MAX_NODEID = 10;
const uint8_t INVALID_NODEID = -1;
command_t lastCommand[MAX_NODEID];
uint32_t receiveTimestamp[MAX_NODEID];
command_t receivedCommand;



void checkRadio();
void setup() {
  	Serial.begin(115200);
  	radio.begin();

  	// Open a writing and reading pipe on each radio, with opposite addresses
  	radio.openWritingPipe(nodesAddress);
  	radio.openReadingPipe(1, sinkAddress);
  	radio.begin();
  	radio.enableDynamicPayloads();
  	radio.setAutoAck(0);
  	radio.setDataRate(RF24_250KBPS);  
  	radio.setPALevel(RF24_PA_MAX);
  	radio.setChannel(channel);
  	radio.printDetails();


  	pinMode(radioIRQ,INPUT);
  	digitalWrite(radioIRQ, HIGH);
  	attachInterrupt(radioIRQ, checkRadio, FALLING);


  	// Start the radio listening for datadigitalPinToInterrupt
  	radio.startListening();

  	wiFiConfig.setupWiFi();
  	server.begin();
  	Serial.println(WiFi.localIP());

  	for(int i = 0; i < MAX_NODEID; i++){
    	lastCommand[i].nodeid = INVALID_NODEID;
    	receiveTimestamp[i] = 0;
  	}
  	receivedCommand.nodeid = INVALID_NODEID;

	clock.begin();
}

void loop() {
	if(receivedCommand.nodeid != INVALID_NODEID){
		//put some delay here, so the sender has his time before the ack comes
		delay(50);
		noInterrupts();
		radio.stopListening();
		radio.write(&receivedCommand, sizeof(command_t));

		lastCommand[receivedCommand.nodeid] = receivedCommand;
		receiveTimestamp[receivedCommand.nodeid] = clock.getTime();
		receivedCommand.nodeid = INVALID_NODEID;
		
		radio.startListening();
		interrupts();
	}


    WiFiClient client = server.available();
    if (!client) {
    	return;
    }

    String request = client.readStringUntil('\r'); //read only get request (1.line)

    Serial.println(request);

    if(wiFiConfig.handleRequest(request, client)){
    }else if(sendCommandPage.handleRequest(request, client, radio)){
    }else if(request.indexOf("lastCommands") != -1){
		temperatureSensor.requestTemperatures();
  		lastCommand[0].state = temperatureSensor.getTempCByIndex(0) * 100;
  		lastCommand[0].nodeid = 0;
  		lastCommand[0].cmd = 3;
  		receiveTimestamp[0] = clock.getTime();



		SimpleJSONWebpage::beginPage(client);

      	client.print("[");
      	bool first = true;
      	for(int i = 0; i < MAX_NODEID; i++){
        	if(lastCommand[i].nodeid != INVALID_NODEID){
          		if(!first)
            		client.print(",");

          		first = false;
          		char buffer[100];
          		sprintf(buffer, "\"%d;%d;%d;%d;%d%d%d%d\"", lastCommand[i].nodeid, lastCommand[i].cmd, lastCommand[i].state, receiveTimestamp[i] ,lastCommand[i].code[0], lastCommand[i].code[1], lastCommand[i].code[2], lastCommand[i].code[3]);
          		client.print(buffer);
        	}
      	}
      	client.print("]");
    }else{
      	Webpage::beginPage(client);
		char buffer[100];
        sprintf(buffer, "Current gateway time: %d<br>", clock.getTime());
		Webpage::add(client, buffer);
      	Webpage::add(client, wiFiConfig.getSetupLink());
      	Webpage::add(client, "</br>");
		Webpage::add(client, sendCommandPage.getSetupLink());
      	Webpage::add(client, "</br>");
		Webpage::add(client, "<a href=\"lastCommands\">Show the last received commands.</a>");
      	Webpage::finishPage(client);   
    }

   client.flush();
}

void checkRadio() {
	noInterrupts();
  	bool tx, fail, rx;
  	radio.whatHappened(tx, fail, rx);                   // What happened?
  	Serial.println("Interrupt!");

  	// If data is available, handle it accordingly
  	if ( rx ) {
  		radio.read(&receivedCommand, sizeof(command_t));
  	}
  	interrupts();
}

