/*
 * =====================================================================================
 *
 *       Filename:  WateringPump.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  30.03.2016 11:37:23
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef WateringPump_h_
#define WateringPump_h_

#include "../../libraries/Timer.h"

class WateringPump : public Timer{
	public:
		enum{
			wateringTime = 1000
		};
		WateringPump(uint8_t pump_pin):
			Timer(Timer::ONESHOT),
			pump_pin(pump_pin){
		}

		void begin(){
			pinMode(pump_pin, OUTPUT);
		}

		void execute(){
			Serial.println("Stop");
			stopPump();
		}
		
		void stopPump(){
			digitalWrite(pump_pin, 0);
		}

		void pump(long ms){
			analogWrite(pump_pin, 150);
			Timer::start(ms);
			Serial.println("Start");
		}


	private:
		uint8_t pump_pin;
};
#endif
