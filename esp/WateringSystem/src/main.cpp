#include <ESP8266WiFi.h>
#include <WiFiServer.h>
#include "../../libraries/WiFiConfig.h"
#include "WateringPump.h"

extern "C" {
#include "user_interface.h"
}

const char* const SSID = "IoT ESP Config";
char hostName[32] = "ESP01_Templogger";

WiFiServer server(80);
WateringPump pump(0);


/*
 * The config class, the SSID is for accesspoint mode.
 * See in line 63.
 */
WiFiConfig wiFiConfig(SSID);
/*
 * The microcontroller setup.
 */
void setup() {
  	Serial.begin(9600);

  	/*
   	 * Try to configure the wifi network.
   	 * If there is already a configuration,
   	 * it will be loaded otherwise the ESP will configure itself 
   	 * as an Accesspoint with the configured SSID.
   	 */
  	wiFiConfig.begin();

  	/*
   	 * This is the webserver which is started by us
   	 * now.
   	 */
  	server.begin();

  	Serial.println(WiFi.localIP());


  	pinMode(0, OUTPUT);



}


void loop() {
	//pump.pump(5000);
  	digitalWrite(0, 1);
	delay(2000);
  	digitalWrite(0, 0);
	delay(2000);
	Serial.println("hjkjhkjh");

	return;
	/*
	 * Is there any client which had a request?
	 * A client is basically everything which accesses 
	 * out web server.
	 */
	WiFiClient client = server.available();
	if (!client) {
		/*
 	 	 * If there is no client we will just
 	 	 * return.
 	 	 */
  	  	return;
	}

	/*
	 * Here we are only interested in the first line of the request.
	 */
	GetRequest getRequest(client);
	String requestString = getRequest.getRequestString(); //read only get request (1.line)

	/*
	 * Well print the line to the serial port for debugging.
	 */
	Serial.println(requestString);

	/*
	 * If the request is a wifi handle request
	 * (http://server/networkConfigDialog)
	 * then we will show the corresponding config page
	 * see WiFiConfig.h, otherwise we see what
	 * the client wants, e.g., show the state of the 
	 * relais.
	 */
	Webpage page(client);
	if(wiFiConfig.handleRequest(getRequest, page)){
	}else{
		page.add("<h1>The available links are.</h1>");
		wiFiConfig.addSetupLink(page);
 	}    
}


