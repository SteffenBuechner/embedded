#include <ESP8266WiFi.h>
#include <WiFiServer.h>
#include "../../libraries/WiFiConfig.h"

const char* const SSID = "IoT ESP Config";

WiFiServer server(80);

/*
 * The config class, the SSID is for accesspoint mode.
 * See in line 63.
 */
WiFiConfig wiFiConfig(SSID);

const int relaisPin = 5;

const int buttonIRQ = 0;


const unsigned long intWait = 100;
unsigned long startInt = 0;


/*
 * Forward reference to the interrupt service routine.
 */
void toggleRelais();

/*
 * The microcontroller setup.
 */
void setup() {
  Serial.begin(115200);

  /*
   * Prepare the relay.
   * The GPIO (General Purpose Input Output)
   * has to be configured as an output, because
   * we want to set it from out software.
   */
  pinMode(relaisPin, OUTPUT);
  /*
   * Inititally we set the output to LOW
   */
  digitalWrite(relaisPin, LOW);

  /*
   * The button shall be an input.
   * There are two ways of using an input,
   * either by polling, i.e., we continuosly read
   * the value, or interrupt driven, i.e., an interrupt
   * is triggered every time the button is pressen.
   * We will use it interrupt driven here, for this
   * the GPIO must be configured as input.
   */
  pinMode(buttonIRQ, INPUT);
  /*
   * This is a thing I didn't understand completely by now.
   * Writing a high to the input "enables" an
   * "pull up" resistor. You have to read about it and then tell
   * me. :))
   */
  digitalWrite(buttonIRQ, HIGH);
  /*
   * Here the actual interrupt is installed. We "attach" a
   * routine to the pin, which is called whenever the interrupt
   * triggers.
   *
   * Interrupts trigger on edges: 
   * Rising - The value changed from Low to High.
   * Falling - Supprise, the value changes from high to low.
   * Change - the value changes either way.
   */
  attachInterrupt(buttonIRQ, toggleRelais, RISING);

  /*
   * Try to configure the wifi network.
   * If there is already a configuration,
   * it will be loaded otherwise the ESP will configure itself 
   * as an Accesspoint with the configured SSID.
   */
  wiFiConfig.setupWiFi();

  /*
   * This is the webserver which is started by us
   * now.
   */
  server.begin();



  Serial.println(WiFi.localIP());
}

void loop() {
	/*
	 * Is there any client which had a request?
	 * A client is basically everything which accesses 
	 * out web server.
	 */
	WiFiClient client = server.available();
	if (!client) {
		/*
 	 	 * If there is no client we will just
 	 	 * return.
 	 	 */
  	  	return;
	}

	/*
	 * Here we are only interested in the first line of the request.
	 */

	GetRequest getRequest( client );//.readStringUntil('\r'));

	Webpage page(client);
	page.beginPage();

	/*
	 * If the request is a wifi handle request
	 * (http://server/networkConfigDialog)
	 * then we will show the corresponding config page
	 * see WiFiConfig.h, otherwise we see what
	 * the client wants, e.g., show the state of the 
	 * relais.
	 */
	if(wiFiConfig.handleRequest(getRequest, page)){
     	 
	}else{
  	//  client.flush();
  	  /*
  	   * Here we look closer into the request.
  	   * Depending on what we find, we write 
  	   * another output to the client.
  	   */
  	  if(getRequest.isRequestFor("/relais/on")){
    	page.add("Relais is now on.");
    	digitalWrite(relaisPin, HIGH);
  	  } else if(getRequest.isRequestFor("/relais/off")){
    	page.add("Relais is now off.");
    	digitalWrite(relaisPin, LOW);
  	  } else if(getRequest.isRequestFor("/relais/read")){
    	if(digitalRead(relaisPin) == LOW){
      	  page.add("Relais is closed.");
    	}else{
      	  page.add("Relais is open.");          
    	}        
  	  } else {
  	  	  page.add("<h1>This device offers</h1>");
  	  	  page.addLink("/relais/on", "Turn relais on");
  	  	  page.addBreak();
  	  	  page.addLink("/relais/off", "Turn relais off");
  	  	  page.addBreak();
  	  	  page.addLink("/relais/read", "Read relay state");
  	  	  page.addBreak();
  	  	  page.addBreak();

  	  	  wiFiConfig.addSetupLink(page);

  	  }
	}    

	page.finishPage();
}


/*
 * The interrupt service routine.
 */
void toggleRelais() {
	/*
	 * Buttons (and other devices) have the ugly 
	 * behaviour of bouncing, i.e., even if the button
	 * is pressed only once, the interrupt triggers 
	 * several times. This can be solved by hardware,
	 * e.g., a filter, or in easily by software.
	 * The software solution just has a timeout
	 * in which every interrupt is ignored.
	 */
	if (millis() - startInt < intWait)
		return;
	startInt = millis();

	/*
	 * read the relay state an toggle the input.
	 */
	int relaisState = digitalRead(relaisPin);

	if (relaisState == LOW)
		relaisState = HIGH;
	else
		relaisState = LOW;

	digitalWrite(relaisPin, relaisState);
	Serial.println("Button pressed.");
}

/*
 * Happy tinkering!!!
 */


