/*
 * =====================================================================================
 *
 *       Filename:  Clock.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  25.03.2016 14:31:44
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef Clock_h_
#define Clock_h_

#include "NTPClient.h"
#include "Timer.h"

class Clock : Timer {
public:
	enum{
		secondsPerDay = 60*60*24
	};

	Clock(uint16_t ntpPort) : 
		Timer(Timer::REPEAT),
		ntpClient(ntpPort),
		uts(0),
		secondsSinceUpdate(0),
		waitingForUpdate(false)
	{	
		
	}

	void execute(){
		secondsSinceUpdate++;
		uts++;

		if(waitingForUpdate){
			uint32_t update = ntpClient.getTimeForLastRequest();
			if(update != 0){
				uts = update;
				waitingForUpdate = false;
			}else if(secondsSinceUpdate > 10){
				secondsSinceUpdate = secondsPerDay;
			}
		}

		if(secondsSinceUpdate == secondsPerDay){
			waitingForUpdate = true;
			ntpClient.sendRequest();	
			secondsSinceUpdate = 0;
		}
	}	

	uint32_t getTime(){
		return uts;
	}

	void begin(){
		ntpClient.begin();
		secondsSinceUpdate = 0;
		waitingForUpdate = true;
		ntpClient.begin();
		Timer::start(1000);
	}

private:
	NTPClient ntpClient;
	uint32_t uts;
	uint32_t secondsSinceUpdate;
	bool waitingForUpdate;
};


#endif
