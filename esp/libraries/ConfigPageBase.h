#ifndef ConfigPageBase_h_
#define ConfigPageBase_h_

#include <ESP8266WiFi.h>
#include <WiFiServer.h>
#include <FS.h>
#include "Webpage.h"
//#define DEBUG_CONF_TOOL

class ConfigPageBase{
	private:
		const char* serviceName;
		char serviceSetup[100];
		char serviceConfigure[100];

		const char* configFileName;

		bool initialized_;
		void setInitialized(bool init){
			initialized_ = init;
		}

	public:
		bool isInitialized(){
			return initialized_;
		}


		virtual void resetConfig() = 0;
	
		template<class config_type>
		bool loadConfig(config_type &conf) {
  			if(!SPIFFS.begin()){
  				resetConfig();
				return false;
			}

  			File configFile = SPIFFS.open(getConfigFileName(), "r");
  			if (!configFile) {
#ifdef DEBUG_CONF_TOOL
				Serial.println("Failed to open config file");
#endif

				setInitialized(false);
				resetConfig();

    			return false;
  			}

  			size_t size = configFile.size();
  			if (size != sizeof(config_type)) {

#ifdef DEBUG_CONF_TOOL
			Serial.print("Config file has wrong size");
#endif

				resetConfig();
				setInitialized(false);
				return false;
  			}

  			configFile.read((uint8_t*)&conf, sizeof(config_type));

#ifdef DEBUG_CONF_TOOL
			Serial.print("Loaded Pushserver config:");
#endif

			setInitialized(true);
  			return true;
		}

		template<class config_type>
		bool saveConfig(config_type &conf) {
  			if(!SPIFFS.begin())
				return false;
  			File configFile = SPIFFS.open(getConfigFileName(), "w");
  			if (!configFile) {
#ifdef DEBUG_CONF_TOOL
    			Serial.println("Failed to open config file for writing");
#endif
    			return false;
  			}

  			configFile.write((uint8_t*)&conf, sizeof(config_type));
  			configFile.close();

#ifdef DEBUG_CONF_TOOL
			Serial.print("Loaded Pushserver config:");
#endif

			setInitialized( true );
  			return true;
		}

  		ConfigPageBase(const char *serviceName, const char *configFileName) : serviceName(serviceName), configFileName(configFileName), initialized_(false){
 			sprintf(serviceSetup, "/setup%s", serviceName);
   			sprintf(serviceConfigure, "/config%s", serviceName);
  	  	}

		virtual bool handleConfigAnswer(GetRequest &request, Webpage &page) = 0;
	

  		const char * getConfigFileName(){
  			return configFileName;
  		}

  		const char * getSetupCommand(){
  			return serviceSetup;
  		}

  		const char *getConfigureCommand(){
  			return serviceConfigure;
  		}

  		const char *getServiceName(){
  			return serviceName;
  		}

  	  	void addSetupLink(Webpage &page){
			char linkDesc[50];
			sprintf(linkDesc, "Setup %s", serviceName);

  	  		page.addLink(getSetupCommand(), linkDesc);
  	  	}


  		bool isConfigAnswer(GetRequest &request){
			return request.isRequestFor(getConfigureCommand());
  		}
  		bool isConfigRequest(GetRequest &request){
			return request.isRequestFor(getSetupCommand());
  		}

  		virtual void showConfig(Webpage &page) = 0;
  		
  		bool handleRequest(GetRequest &request, Webpage &page){
			if(isConfigRequest(request)){
				showConfig(page);
				return true;	
			}else if(isConfigAnswer(request)){
				if(handleConfigAnswer(request, page)){
				}
				return true;
			}

			return false;
  		}


};

#endif
