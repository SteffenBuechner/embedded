/*
 * =====================================================================================
 *
 *       Filename:  GetRequest.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  08.04.2016 14:16:05
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef GetRequest_h_
#define GetRequest_h_

//#define DEBUG_GETREQUEST

#include "string.h"

class GetRequest{
		String request;
		String requestFor;
	public:
		GetRequest(WiFiClient &client){
			request = client.readStringUntil(0);

#ifdef DEBUG_GETREQUEST
			Serial.println("Request begin:");
			Serial.print(request);
#endif

			if(!request.startsWith("GET "))
				return; //no request at all
			
			int startPos = 4;
			int endPos = request.indexOf("?", startPos);
			if(endPos == -1)
				endPos = request.indexOf(" ", startPos);

			requestFor = request.substring(startPos, endPos);
#ifdef DEBUG_GETREQUEST
			Serial.print("Request for:");
			Serial.println(requestFor);
			Serial.println("Request end.");
#endif
		}

		bool fieldExists(const char *name){
			int startPos = -1;
			
			while(true){
				startPos = request.indexOf(name, startPos + 1);

				//not found
				if(startPos < 1)
					return false;
	
				if(request[startPos - 1] == '&' || request[startPos - 1] == '?')
					return true;
			}

			return true;

		}

		bool getField(const char *name, char *destination, size_t maxLength){
			int startPos = -1;
			
			while(true){
				startPos = request.indexOf(name, startPos + 1);

				//not found
				if(startPos < 1)
					return false;
	
				if(request[startPos - 1] == '&' || request[startPos - 1] == '?')
					break;
			}


			startPos += strlen(name) + 1;//+1 for =

			int endPos = request.indexOf("&", startPos);
			
			if(endPos == -1)
				endPos = request.indexOf(" ", startPos);

			if(endPos - startPos > maxLength){
				return false;
			}

			memcpy(destination, request.substring(startPos, endPos).c_str(), endPos - startPos);
			destination[endPos-startPos] = 0;

			return true;
		}

		bool contains(const char *str){
			return request.indexOf(str) != -1;
		}

		bool isRequestFor(const char *str){
			return requestFor.equals(str);
		}

		String getRequestString(){
			return request;
		}
};


#endif
