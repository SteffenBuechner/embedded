/*
 * =====================================================================================
 *
 *       Filename:  NTPClient.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  25.03.2016 12:39:23
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef NTPClient_h_
#define NTPClient_h_

#include "Timer.h"
#include <WiFiUdp.h>

class NTPClient : Timer{
	public:
		enum{
			NTP_PACKET_SIZE = 48
		};

		const char* TIMESERVER = "0.de.pool.ntp.org";

		//78.46.93.106
		NTPClient(int16_t port):
			Timer( Timer::ONESHOT),
			address(78,46,93,106),
			port(port)
		{
//			WiFiDrv::getHostByName(TIMESERVER, address);
		}

		void begin(){
			udp.begin(port);
		}

		void execute(){
			int cb = udp.parsePacket();
  			if (!cb) {
				return;
  			}
  			else {
    			// We've received a packet, read the data from it
    			udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

    			//the timestamp starts at byte 40 of the received packet and is four bytes,
    			// or two words, long. First, esxtract the two words:

    			unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    			unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    			// combine the four bytes (two words) into a long integer
    			// this is NTP time (seconds since Jan 1 1900):
				// but unix timestamp starts from 1970! => substract 70years
				const unsigned long seventyYears = 2208988800UL;
    			// subtract seventy years:
    			timeForLastRequest = (highWord << 16 | lowWord) - seventyYears;
			}
		}

		uint32_t getTimeForLastRequest(){
			return timeForLastRequest;
		}

		void sendRequest()
		{
			timeForLastRequest = 0;
  			// set all bytes in the buffer to 0
  			memset(packetBuffer, 0, NTP_PACKET_SIZE);
  			// Initialize values needed to form NTP request
  			// (see URL above for details on the packets)
  			packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  			packetBuffer[1] = 0;     // Stratum, or type of clock
  			packetBuffer[2] = 6;     // Polling Interval
  			packetBuffer[3] = 0xEC;  // Peer Clock Precision
  			// 8 bytes of zero for Root Delay & Root Dispersion
  			packetBuffer[12]  = 49;
  			packetBuffer[13]  = 0x4E;
  			packetBuffer[14]  = 49;
  			packetBuffer[15]  = 52;

  			// all NTP fields have been given values, now
  			// you can send a packet requesting a timestamp:
  			udp.beginPacket(address, 123); //NTP requests are to port 123
  			udp.write(packetBuffer, NTP_PACKET_SIZE);
  			udp.endPacket();

			Timer::start(1000);
		}		
	private:
		IPAddress address;
		WiFiUDP udp;
		uint16_t port;
		uint32_t timeForLastRequest;
		uint8_t packetBuffer[NTP_PACKET_SIZE];

};


#endif
