#ifndef PushServer_h_
#define PushServer_h_

#include <ESP8266WiFi.h>
#include <WiFiServer.h>
#include <FS.h>
#include "Webpage.h"
#include "ConfigPageBase.h"
//#define DEBUG_CONF_TOOL

class PushServer : public ConfigPageBase{
	private:
		enum{
			IPADDRESS_LENGTH = 16
		};

		// Create an instance of the server
  		// specify the port to listen on as an argument

  		struct config_t{
  			enum{
				maxServicePageLength = 50
  			};
			IPAddress ipaddress;
			char servicePage[maxServicePageLength];
  		};

  		config_t config;

		void resetConfig(){
#ifdef DEBUG_CONF_TOOL
			Serial.print("Reset ipaddress to:");
#endif
	
			config.ipaddress.fromString("128.0.0.1");
			config.servicePage[0] = 0;
#ifdef DEBUG_CONF_TOOL
			Serial.println(config.ipaddress.toString());
#endif
		}

		WiFiClient *wiFiClient;

	public:

		IPAddress *getIPAddress(){
			return &config.ipaddress;
		}

  		PushServer(const char *serviceName, const char *configFileName) : 
  			ConfigPageBase(serviceName, configFileName){
  			wiFiClient = 0;
  	  	}

		bool handleConfigAnswer(GetRequest &request, Webpage &page){
			char ipaddress[IPADDRESS_LENGTH];
			bool ok = request.getField("serverip", ipaddress, IPADDRESS_LENGTH);
			char servicepage[config_t::maxServicePageLength];
			ok &= request.getField("servicepage", servicepage, config_t::maxServicePageLength);

			if(ok){
#ifdef DEBUG_CONF_TOOL
        		Serial.print("Field:");
        		Serial.println(ipaddress);
#endif

				config.ipaddress.fromString(ipaddress);
				memcpy(config.servicePage, servicepage, config_t::maxServicePageLength);

				bool saveok = saveConfig(config);

				if(saveok){
					page.add("Configuration updated.");
					return saveok;
				}else{
					page.add("Configuration was not valid.");
					return false;
				}

				return false; 
      		}
      		else{
				page.add("Configuration was not valid.");
      		}

      		return false;
  		}

		void showConfig(Webpage &page){
			page.addHeadline(getServiceName());
			page.beginFormular(getConfigureCommand());
			page.addText("IP Address:");
			page.formAddInputField("text", "serverip",config.ipaddress.toString() );
			page.addText("Service Page:");
			page.formAddInputField("text", "servicepage", config.servicePage );
			page.addBreak();
			page.finishFormular("Setup");
  		}

  		void begin(){
 			loadConfig(config);
  		}	

		bool beginPush(WiFiClient &client, int nodeid){
			if(!isInitialized())
				return false;

  			if ( !client.connect(*getIPAddress(), 80) ) {
    			return false;
  			}
  			
  			wiFiClient = &client;

  			wiFiClient->print("GET /");
  			wiFiClient->print(config.servicePage);
  			wiFiClient->print("?id=");
  			wiFiClient->print(nodeid);
#ifdef DEBUG_CONF_TOOL	
  			Serial.print("GET /");
  			Serial.print(config.servicePage);
  			Serial.print("?id=");
  			Serial.print(nodeid);
#endif

  			return true;
		}

		bool finishPush(){
			if(wiFiClient == 0)
				return false;
 			wiFiClient->println(" HTTP/1.1");
  			wiFiClient->print("Host: ");
  			wiFiClient->println(getIPAddress()->toString());
  			wiFiClient->println("Connection: close");
  			wiFiClient->println();

  			wiFiClient = 0;

#ifdef DEBUG_CONF_TOOL
			Serial.println(" HTTP/1.1");
  			Serial.print("Host: ");
  			Serial.println(getIPAddress()->toString());
  			Serial.println("Connection: close");
  			Serial.println();
#endif

			return true;
	
		}


		template<class Type>
		bool addPush(const char *field, Type value ) {
			if(wiFiClient == 0)
				return false;

  			wiFiClient->print("&");
  			wiFiClient->print(field);
  			wiFiClient->print("=");
  			wiFiClient->print(value);
#ifdef DEBUG_CONF_TOOL
			Serial.print("&");
  			Serial.print(field);
  			Serial.print("=");
  			Serial.print(value);
#endif
 
  			return true;
		}


};

#endif
