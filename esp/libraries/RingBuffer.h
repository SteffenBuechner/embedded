/*
 * =====================================================================================
 *
 *       Filename:  RingBuffer.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  20.03.2016 10:47:41
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef RingBuffer_h_
#define RingBuffer_h_

template<class Type, size_t size>
class RingBuffer{
	public:
		RingBuffer(){
			read = 0;
			write = 0;
		}

		bool add(Type &item){
			int dummy = next(write);
			if(dummy == read)
				return false;

			buffer[write] = item;
			write = dummy;
		}

		int nrOfItems(){
			if(write > read){
				return write - read;
			}

			if(write < read){
				return (size - read) + write;
			}

			return 0;
		}

		bool get(Type &item, int pos){
			if(pos > nrOfItems())
				return false;

			item = buffer[(read + pos) % size];
		}

		void free(int number){
			if(number > nrOfItems()){
				read = 0;
				write = 0;
				return;
			}

			read = (read + number) % size;
		}

		int next(int n){
			return (n + 1) % size;
		}
		
	private:
		int read;
		int write;
		Type buffer[size];
};


#endif
