/*
 * =====================================================================================
 *
 *       Filename:  Timer.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  25.03.2016 13:55:27
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef Timer_h_
#define Timer_h_


#include "os_type.h"
#include <osapi.h>

void timer_func(void *arg);

class Timer{
	public:
		enum Type{
			ONESHOT = 0,
			REPEAT = 1
		};
		virtual void execute() = 0;		

		Timer(Type type):ms(ms), timerType(type){
  			os_timer_disarm(&timer);
    		os_timer_setfn(&timer, (os_timer_func_t*)timer_func, (void *)this);
		}	

		void start(long ms){
    		os_timer_arm(&timer, ms, timerType );		
		}
		
		void stop(){
 			os_timer_disarm(&timer);
		}

	private:
		os_timer_t timer;
		Type timerType;
		uint16_t ms;
};

void timer_func(void *arg){
	((Timer*)arg)->execute();
}


#endif
