/*
 * =====================================================================================
 *
 *       Filename:  Webpage.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  20.03.2016 09:38:16
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef Webpage_h_
#define Webpage_h_

#include <WiFiServer.h>

class Webpage{
	static constexpr const char* const HTML_HEADER = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><body>";
  	static constexpr const char* const HTML_FOOTER = "</html></body>";

 	WiFiClient *wiFiClient;
	public:

  	static void beginPage(WiFiClient &client){
  		client.print(Webpage::HTML_HEADER);
  	}

  	static void finishPage(WiFiClient &client){
  		client.print(Webpage::HTML_FOOTER);
  		client.flush();
  		client.stop();
  	}

	template<class Type>
  		static void add(WiFiClient &client, Type content){
  			client.print(content);
  		}

  	Webpage(WiFiClient &client) : wiFiClient(&client){
  	}

  	~Webpage(){
  	}

	void beginPage(){
		if(wiFiClient != 0)
  			wiFiClient->println(Webpage::HTML_HEADER);
	}

  	void finishPage(){
		if(wiFiClient != 0){
  			wiFiClient->println(Webpage::HTML_FOOTER);
  			wiFiClient->flush();
  			wiFiClient = 0;
  		}
  	}

	void addLink(const char *locaction, const char *description){
		if(wiFiClient != 0){
 	  		wiFiClient->print("<a href=\"");
  	  		wiFiClient->print(locaction);
  	  		wiFiClient->print("\">");
  	  		wiFiClient->print(description);
  	  		wiFiClient->println("</a>");
  	  	}
	}

	void beginFormular(const char* destination){
  	  	wiFiClient->print("<form action=\"");
  	  	wiFiClient->print(destination);
  	  	wiFiClient->println("\" method=\"get\">");
	}

	void addText(const char *text, bool breakLine = true){
		add(text);
		if(breakLine)
			addBreak();
	}

	template<class Type>
	void formAddInputField(const char *type, const char *fieldname, Type value, bool breakLine = true){
  	  	wiFiClient->print("<input type=\"");
  	  	wiFiClient->print(type);
  	  	wiFiClient->print("\" name=\"");
  	  	wiFiClient->print(fieldname);
  	  	wiFiClient->print("\" value=\"");
  	  	wiFiClient->print(value);
  	  	wiFiClient->print("\">");

  	  	if(breakLine)
  	  		addBreak();
	}

	template<class Type>
	void formCheckBoxField( const char *fieldname, Type value, bool checked, bool breakLine = true){
  	  	wiFiClient->print("<input type=\"checkbox\" name=\"");
  	  	wiFiClient->print(fieldname);
  	  	wiFiClient->print("\" value=\"");
  	  	wiFiClient->print(value);
  	  	if(checked)
  	  		wiFiClient->print("\" checked >");
  	  	else
  	  		wiFiClient->print("\">");

  	  	if(breakLine)
  	  		addBreak();
	}

	void finishFormular(const char *buttonText){
		wiFiClient->print("<input type=\"Submit\" value=\"");
		wiFiClient->print(buttonText);
		wiFiClient->println("\"></form>");
	}

	void addHeadline(const char *text){
		wiFiClient->print("<h1>");
		wiFiClient->print(text);
		wiFiClient->print("</h1>");
	}

	void addBreak(){
		add("</br>");
	}

	template<class Type>
  		void add(Type content){
			if(wiFiClient != 0){
  				wiFiClient->println(content);
  			}
  		}
};

#endif
