#ifndef WiFiConfig_h_
#define WiFiConfig_h_

#include <ESP8266WiFi.h>
#include <WiFiServer.h>
#include <FS.h>

#include "GetRequest.h"
#include "ConfigPageBase.h"
#include "Webpage.h"

#define DEBUG_CONF_TOOL

class WiFiConfig : public ConfigPageBase{
	private:
  		// Create an instance of the server
  		// specify the port to listen on as an argument
		enum{
			IPADDRESS_LENGTH = 16
		};


  		const char *configSSID;

 		struct config_t{
 			enum{
				maxFieldLength = 20
 			};
  			void setTerminators(){
  				ssid[maxFieldLength-1] = 0;
  				psk[maxFieldLength-1] = 0;
  				hostname[maxFieldLength-1] = 0;
  			}
  			void reset(){
 				ssid[0] = 0;
  				psk[0] = 0;
  				hostname[0] = 0;
  				useStaticIP = false;
  				
  				staticIP = WiFi.localIP();
  				subnetMask.fromString("255.255.255.0");
  				gatewayIP = staticIP;
  				gatewayIP[3] = 1;
  				dnsIP = staticIP;
  				dnsIP[3] = 1;
  			}
			char ssid[maxFieldLength];
  			char psk[maxFieldLength];
  			char hostname[maxFieldLength];
  			IPAddress staticIP;
  			IPAddress subnetMask;
  			IPAddress gatewayIP;
  			IPAddress dnsIP;
  			bool useStaticIP;
  			uint16_t id;
  		};

		config_t config;


		void resetConfig(){
			config.reset();
		}
	public:
  		WiFiConfig(const char *ssid):
  			ConfigPageBase("network", "/config.wifi"),
  	  		configSSID(ssid){
  	  			config.reset();
  			}

  	  	void addSetupLink(Webpage &page){
  	  		page.addLink(getSetupCommand(), "Setup WiFi Parameters");
  	  	}

  		/*
   		 * Try to connect to WiFi
   		 * if not succeeding go to AP mode
   		 */
  		bool connectToWiFiNetwork(){
  			WiFi.disconnect();
  			WiFi.softAPdisconnect();
    		WiFi.mode(WIFI_STA); 

    		if(config.useStaticIP){
				WiFi.config(config.staticIP,  config.dnsIP, config.gatewayIP, config.subnetMask);
    		}

    		WiFi.begin(config.ssid, config.psk);

    		char retries = 25;
    		while ((WiFi.status() != WL_CONNECTED) && ((--retries) != 0)) {
      			delay(500);
    		}
    		return WiFi.status() == WL_CONNECTED;
  		}

  		bool handleConfigAnswer(GetRequest &request, Webpage &output){
  			config_t dummy;
  			dummy.reset();

			bool ok = request.getField("SSID", dummy.ssid, config_t::maxFieldLength );
			ok &= request.getField("PSK", dummy.psk, config_t::maxFieldLength);
			ok &= request.getField("HOSTNAME", dummy.hostname, config_t::maxFieldLength);
		
			char str_id[10];
			ok &= request.getField("ID", str_id, 10);

		
			//there is only one checkbox if the field exists it means that it was checked..
			char checkBoxResult[2];
			dummy.useStaticIP = request.getField("useStaticIP", checkBoxResult, 2);

			char ipaddress[IPADDRESS_LENGTH];
			bool incompleteStaticIP = false;
			if(request.getField("staticIP", ipaddress, IPADDRESS_LENGTH)){
				dummy.staticIP.fromString(ipaddress);
			}else{
				incompleteStaticIP = true;
			}
			
			if(request.getField("subnetMask", ipaddress, IPADDRESS_LENGTH)){
				dummy.subnetMask.fromString(ipaddress);
			}else{
				incompleteStaticIP = true;
			}

			if(request.getField("gateway", ipaddress, IPADDRESS_LENGTH)){
				dummy.gatewayIP.fromString(ipaddress);
			}else{
				incompleteStaticIP = true;
			}
			if(request.getField("dns", ipaddress, IPADDRESS_LENGTH)){
				dummy.dnsIP.fromString(ipaddress);
			}else{
				incompleteStaticIP = true;
			}

			if(ok){
				config = dummy;
				config.id = atoi(str_id);

				#ifdef DEBUG_CONF_TOOL
        		Serial.println("---------");
        		Serial.println(config.ssid);
        		Serial.println(config.psk);
        		Serial.println(config.hostname);
        		Serial.println(config.id);
        		Serial.println("---------");
				#endif

				bool saveOk = saveConfig(config);

				if(config.useStaticIP && incompleteStaticIP){
					config.useStaticIP = false;
					output.addText("Static IP configuration is incomplete. The static IP will not be used.");
				}


				if(saveOk){
					output.add("Configuration updated.");
					output.finishPage();
					return true;
				}else
					output.add("Saving the configuration failed.");

				connectToWiFiNetwork();

				return saveOk; 
			}

			output.add("This was not a valid Configuration.");

			return false;
  		}

  		void showConfig(Webpage &page){
			page.addHeadline("WiFi Parameter Configuration");
			page.beginFormular(getConfigureCommand());
			page.addText("SSID:");
			page.formAddInputField("text", "SSID",config.ssid );
			page.addText("PSK:");
			page.formAddInputField("text", "PSK", "" );
			page.addText("Hostname:");
			page.formAddInputField("text", "HOSTNAME", config.hostname );
			page.addText("ID:");
			char nodeid[5];
			sprintf(nodeid, "%d", config.id);
			page.formAddInputField("text", "ID", nodeid  );

			page.addText("Static IP Configuration");
			page.addBreak();
			page.addText("Use a static IP", false);
			page.formCheckBoxField("useStaticIP", "1", config.useStaticIP);

			page.addText("Static IP:");
			page.formAddInputField("text", "staticIP",config.staticIP );
			page.addText("Subnet Mask:");
			page.formAddInputField("text", "subnetMask", config.subnetMask );
			page.addText("DNS:");
			page.formAddInputField("text", "dns", config.dnsIP );
			page.addText("Gateway:");
			page.formAddInputField("text", "gateway", config.gatewayIP );
	
			page.addBreak();
			page.finishFormular("Setup");
  		
  		}

		const char *getHostname(){
			return config.hostname;
		}

		int getNodeID(){
			return config.id;
		}

		bool begin(){
			return setupWiFi();
		}

  		bool setupWiFi(){
			bool configOk = loadConfig(config);
      		// Connect to WiFi network
    		if(!configOk || !connectToWiFiNetwork()){
    			WiFi.disconnect();
    			WiFi.softAPdisconnect();
	  			WiFi.mode(WIFI_AP);
      			WiFi.softAP(configSSID);
      			WiFi.begin();
				#ifdef DEBUG_CONF_TOOL
      			Serial.print("AP mode IP:");
      			Serial.println(WiFi.softAPIP());
				#endif
				return false;
			}
#ifdef DEBUG_CONF_TOOL
			Serial.println(config.ssid);
			Serial.println(config.psk);
			Serial.println(config.hostname);
			Serial.println(config.id);
			Serial.println(config.staticIP.toString());
			Serial.println(config.subnetMask.toString());
			Serial.println(config.dnsIP.toString());
			Serial.println(config.gatewayIP.toString());
#endif


			return true;
  		}
};

#endif
